from acp_times import open_time
from acp_times import close_time

import arrow
import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


start = arrow.get("2000-01-01T00:00")

# 200km tests

def test_100km_close_time():
    assert (close_time(100, 200, start) == start.replace(hours=+ 6, minutes=+40))

def test_0km_close_time():
    assert (close_time(0, 200, start) == start.replace(hours=+ 1))

def test_60km_close_time():
    assert (close_time(60, 200, start) == start.replace(hours=+ 4))

# 1000km tests

def test_999km_close_time():
    assert (close_time(999, 1000, start) == start.replace(hours=+ 98, minutes=+55))

def test_0km_close_time():
    assert (close_time(0, 1000, start) == start.replace(hours=+ 1))

def test_60km_close_time():
    assert (close_time(1100, 1000, start) == start.replace(hours=+ 99))
