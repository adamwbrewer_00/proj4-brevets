docker stop $(docker ps -a -q) 2>/dev/null
docker rm $(docker ps -a -q) 2>/dev/null
docker images | grep none | awk '{ print $3; }' | xargs docker rmi
